var express = require('express');
var router = express.Router();
var path = require('path');
var fs = require('fs');

router.get('/', function(req, res, next) {
  res.sendFile(path.resolve(__dirname + '/../storage/quotes.json'))
});

router.post('/', function(req, res, next) {
  fs.readFile(__dirname + '/../storage/quotes.json', (err, data) => {
    data = JSON.parse(data)
    data.quotes.push(req.body)
    fs.writeFile(__dirname + '/../storage/quotes.json', JSON.stringify(data))
  });
  res.end()
});

router.delete('/', function(req, res, next) {
  fs.readFile(__dirname + '/../storage/quotes.json', (err, data) => {
    data = JSON.parse(data)
    data.quotes = data.quotes.filter((quote) => quote.id != req.query.id)
    fs.writeFile(__dirname + '/../storage/quotes.json', JSON.stringify(data))
  });
  res.end()

});

module.exports = router;
