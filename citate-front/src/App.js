import React, { Component } from 'react';
import './App.css';

const maxQuotes = 10;
const apiUrl = 'http://139.59.137.126/quotes'

let showAndHideSuccessMessage = function() {
  document.getElementById('successMessage').style.visibility = 'visible'
  setTimeout(function() {
    document.getElementById('successMessage').style.visibility = 'hidden'
  }, 1000)
}

class App extends Component {
  constructor() {
    super()
    this.addQuote = this.addQuote.bind(this)
    this.removeQuote = this.removeQuote.bind(this)
    this.getQuotes = this.getQuotes.bind(this)
    this.state = {
      quotes: []
    }
    this.getQuotes()
  }

  getQuotes() {
    let self = this
    let xhr = new XMLHttpRequest()
    xhr.open('GET', apiUrl, true)
    xhr.onreadystatechange = function() {
      if (this.readyState === 4 && this.status === 200) {
        self.setState({
          quotes: JSON.parse(this.responseText).quotes
        })
      }
    }
    xhr.send()
  }

  addQuote() {
    if (this.state.quotes.length >= maxQuotes) {
      alert("Для добавления новых цитат удалите одну из добавленных")
    } else {
      let self = this
      let xhr = new XMLHttpRequest()
      let data = {
        id: Date.now(),
        text: document.getElementById('quote_textarea').value
      }
      if (!data.text) return
      xhr.open('POST', apiUrl, true)
      xhr.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
          self.setState({
            quotes: self.state.quotes.concat([data])
          })
          showAndHideSuccessMessage()
          document.getElementById('quote_textarea').value = ''
        }
      }
      xhr.setRequestHeader("Content-Type", "application/json");
      xhr.send(JSON.stringify(data))
    }
  }

  removeQuote(quoteId) {
    let self = this
    let xhr = new XMLHttpRequest()
    xhr.open('DELETE', apiUrl + '?id=' + quoteId, true)
    xhr.onreadystatechange = function() {
      if (this.readyState === 4 && this.status === 200) {
        self.setState({
          quotes: self.state.quotes.filter((quote) => quote.id !== quoteId)
        })
      }
    }
    xhr.send()
  }

  render() {
    return (
      <div className="App">
        <div id="successMessage">Quotes Added</div>
        <div className="progress">
          <div className="progress-bar" role="progressbar" style={{width: (this.state.quotes.length*100/maxQuotes) + '%'}}>
            {this.state.quotes.length}/10
          </div>
        </div>
        <div className="submitForm">
          <div className="form-group">
            <label htmlFor="quote_textarea">Quote</label>
            <textarea className="form-control" id="quote_textarea"></textarea>
          </div>
          <button className="btn btn-primary" onClick={this.addQuote}>Add Quote</button>
        </div>
        <div className='quotes'>
          {this.state.quotes.map((quote) =>
            <div className="panel panel-default" key={quote.id}>
              <button type="button" onClick={this.removeQuote.bind(this, quote.id)} className="close"><span>&times;</span></button>
              <div className="panel-body">{quote.text}</div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default App;
